package jp.co.junit4.sample;

public class FizzBuzz {

	public static void main(String[] args) {
		for(int i = 1; i <= 100; i++) {
			System.out.println(checkFizzBuzz(i));
		}
	}

	public static String checkFizzBuzz(int num) {
		//if num % 3 == 0 => Fizz
		String fizz = "Fizz";
		//if num % 5 == 0 => Buzz
		String buzz = "Buzz";
		//num to string
		String num_str = Integer.toString(num);
		//ready for print out result
		String result = null;
		if (num % 3 == 0 && num % 5 == 0) {
			result = fizz+buzz;
		} else if (num % 3 == 0) {
			result = fizz;
		} else if (num % 5 == 0) {
			result = buzz;
		} else {
			result = num_str;
		}
		return result;
	}
}
