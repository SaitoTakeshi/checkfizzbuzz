/**
 *
 */
package jp.co.junit4.sample;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author saito.takeshi
 *
 */
class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * {@link jp.co.junit4.sample.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 * (引数:9)
	 */
	@Test
	void testFizz() {
		System.out.println("testFizz");
		assertEquals("Fizz",FizzBuzz.checkFizzBuzz(9));
	}

	/**
	 * {@link jp.co.junit4.sample.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 * (引数:20)
	 */

	@Test
	void testBuzz() {
		System.out.println("testBuzz");
		assertEquals("Buzz",FizzBuzz.checkFizzBuzz(20));
	}
	/**
	 * {@link jp.co.junit4.sample.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 * (引数:45)
	 */

	@Test
	void testFizzBuzz() {
		System.out.println("testFizzBuzz");
		assertEquals("FizzBuzz",FizzBuzz.checkFizzBuzz(45));
	}

	/**
	 * {@link jp.co.junit4.sample.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 * (引数:44)
	 */
	@Test
	void testNum1() {
		System.out.println("testNum1");
		assertEquals("44",FizzBuzz.checkFizzBuzz(44));
	}

	/**
	 * {@link jp.co.junit4.sample.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 * (引数:46)
	 */
	@Test
	void testNum2() {
		System.out.println("testNum2");
		assertEquals("46",FizzBuzz.checkFizzBuzz(46));
	}

}
